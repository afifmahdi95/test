FROM maven:3-jdk-8

RUN mkdir -p /app && chmod -R 777 /app
WORKDIR /app

COPY . /app

RUN mvn clean package -Dmaven.test.skip=true

EXPOSE 8001

CMD [ "java", "-jar", "-Dserver.port=8001", "/app/target/demo-0.0.1-SNAPSHOT.war", "--server.servlet.context-path=/test"]
