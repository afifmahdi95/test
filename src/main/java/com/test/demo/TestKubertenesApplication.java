package com.test.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class TestKubertenesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestKubertenesApplication.class, args);
    }

}
