package com.test.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

@RestController
@RequestMapping(value = "/test-demo/", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "API untuk test")
public class TestController {
    @GetMapping("test-kubetenes")
    @ApiOperation(value = "Test kubertenes")
    public ResponseEntity browseDocument() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        try {
            LinkedHashMap<String, String> data = new LinkedHashMap<>();
            data.put("Nama","Test");
            data.put("Umur","99");
            data.put("Alamat", "Cempaka Timur, Jakarta Pusat");

            res.put("status", true);
            res.put("message", "Status Success");
            res.put("data", data);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());

            return ResponseEntity.badRequest().body(res);
        }
    }
}
